<?php

if (!isset($_SESSION)) session_start();


// function dd($object)
// {   
//     var_dump($object);
//     die('Killing it!');
// }

function pr($array)
{
    print_r($array);
}

function html_head()
{
    include_once 'layout/header.php';
}

function html_footer()
{
    include_once 'layout/footer.php';
}

function post_request($key)
{
    if(isset($_POST[$key])){
        return $_POST[$key];
    }
    return false;
}

function file_request($key)
{
    return $_FILES[$key];
}

function set_session_flash($name,$value)
{
    $_SESSION[$name] = $value;
}

function get_session_flash($name)
{
    if(isset($_SESSION[$name])){
        $value =  $_SESSION[$name];
        unset($_SESSION[$name]);
        return $value;
    }
    return null;
}

function true_or_die($condition,$message = null)
{
    if($condition !== true) die($message);
}

function true_or_redirect($condition,$url)
{
    if($condition !== true){
        header("Location: {$url}");
    }
}


function create_hash($len=32)
{
	return substr(md5(openssl_random_pseudo_bytes(20)),-$len);
}

function use_token()
{
    $verificationCode = create_hash();
    set_session_flash('verification_code',$verificationCode);
    echo '<input type="hidden" name="verification_code" value="'.$verificationCode.'" />';
    
}

function check_token()
{
    return (get_session_flash('verification_code') === post_request('verification_code'));
}


function move_upload($fileName){
    $folder = "";
    $uploads_dir = "/{$folder}";
    $tmp_name = $_FILES["file"]["tmp_name"][0];
    move_uploaded_file($tmp_name, "{$fileName}");
}


function validate_data($rule,$value){

    $rules = [
        'serial_number'     => "/^[\d]{3,15}$/",
        'activation_key'    => "/^[a-zA-Z$0-9.\/]{0,100}$/",
        'phone'             => "/^[0-9]{3}\.?\-?[0-9]{3}\.?\-?[0-9]{4}$/",
        'email'             => "/^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$/",
        'database_column'   =>  "/^[a-zA-Z 0-9_]*$/",
        'url'               => "/\(?(?:(http|https|ftp):\/\/)?(?:((?:[^\W\s]|\.|-|[:]{1})+)@{1})?((?:www.)?(?:[^\W\s]|\.|-)+[\.][^\W\s]{2,4}|localhost(?=\/)|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::(\d*))?([\/]?[^\s\?]*[\/]{1})*(?:\/?([^\s\n\?\[\]\{\}\#]*(?:(?=\.)){1}|[^\s\n\?\[\]\{\}\.\#]*)?([\.]{1}[^\s\?\#]*)?)?(?:\?{1}([^\s\n\#\[\]]*))?([\#][^\s\n]*)?\)?/gi;"
    ];

    preg_match($rules[$rule], $value, $output);
    return (!empty($output)) ? true : false;
}


function message($message = null){
    
    if(isset($_SESSION['message'])){
        $value =  $_SESSION['message'];
        unset($_SESSION['message']);
        return $value;
    }else{
        return $_SESSION['message'] = $message;
    }   

}

function redirect($url){
    true_or_die( validate_data('url',$url) );
    header("Location: {$url}");
}

function route($url){
    echo "{$url}.php";
}