<?php

namespace FabricioBiron\CSVMapper;

use ParseCsv\Csv;
use FabricioBiron\CSVMapper\StoredCSV;

class CSVImport{

    public $sourceFile;
    public $sourceRecords;
    public $headers;
    public $sourceHeaders;
    public $map;
    public $csv;

    public function __construct($file = null)
    {
        $this->headers = [
            "sku",
            "attribute_set_code",
            "product_type",
            "categories",
            "product_websites",
            "name",
            "description",
            "tax_class_name",
            "visibility",
            "price",
            "meta_title",
            "qty",
            "base_image",
            "base_image_label",
            "small_image",
            "small_image_label",
            "thumbnail_image",
            "thumbnail_image_label"
        ];
        
        $this->csv = new Csv($file);
        
    }

    /**
     * Get all existent records in a given CSV file
     *
     * @return array $data
     */
    public function getAll()
    {
        $records = $this->csv->data;
        $this->setSourceHeaders(array_keys($this->csv->data[0]));
        $data = $records;
        return $data;
    }

    /**
     * Undocumented function
     *
     * @param array $headers
     * @return void
     */
    public function setHeaders($headers)
    {
        $explodedHeader = explode(',',$headers);

        $mapHeaders = array_map(function($val){
            return ( validate_data('database_column',trim($val)) ) ? trim($val) : '';
        },$explodedHeader);

        $this->headers = $mapHeaders;
    }


    public function getHeaders()
    {
        return $this->headers;
    }

    public function setSourceHeaders($array)
    {   
        $this->sourceHeaders = $array;
    }

    public function getSourceHeaders(){
        return $this->sourceHeaders;
    }

    /**
     * Undocumented function
     * @todo $order should come as a parameter 
     * @return void
     */
    public function setMap($fieldList)
    {
        $amountOfColumns = count($this->headers);
        $source = $fieldList;

        // This following statements are to fill up the array so that it is possible to combine headers
        $amountOfColumnsInSource = count($source);
        $leftOver = $amountOfColumns - $amountOfColumnsInSource;
        $count = 1;
        while($count <= $leftOver){
            $source[] = '';
            $count++;
        }

        $this->map = array_combine($this->headers,$source);
    }

    public function getMap()
    {
        return $this->map;
    }

    public function mapResults()
    {
        $rows = $this->getAll();
        $map = $this->getMap();
        $index = 0;
        $data = [];

        foreach ( $rows as $row )
        {   
            foreach ( $map as $standardColumn=>$sourceColumn )
            {   
                if($sourceColumn !== ''){
                    $data[$index][$standardColumn] = $this->normalize($sourceColumn, $row[$sourceColumn]);     
                }else{
                    $data[$index][$standardColumn] = '';     
                }
            }
            $index++;
        } 

        return $data;

    }

    public function normalize($key, $item)
    {       
        switch ($key)
        {
            case 'base_image_label':
                $result = explode('.',$item);
                return $result[0];
                break;
        }

        return $item;
    }

}