<?php

namespace FabricioBiron\CSVMapper;

if (!isset($_SESSION)) session_start();

class StoredCSV{

    public static function get()
    {
        return $_SESSION['CSV'];   
    }

    public static function set($value){
        $_SESSION['CSV'] = $value;
    }

}