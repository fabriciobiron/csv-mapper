<?php

require 'vendor/autoload.php';

use FabricioBiron\CSVMapper\CSVImport;
use FabricioBiron\CSVMapper\StoredCSV;

true_or_redirect(check_token(),'index.php');

$csv = StoredCSV::get();
$csv->setMap( post_request('order') );

$data = $csv->mapResults();
$headers= $csv->getHeaders();

$csv->csv->output('csvfile.csv', $data, $headers, ',');

?>