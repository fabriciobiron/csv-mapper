<?php

require 'vendor/autoload.php';

use FabricioBiron\CSVMapper\CSVImport;
use FabricioBiron\CSVMapper\StoredCSV;


$fileFromUpload = $_FILES["file"]["tmp_name"];
$csvImportedContent = file_get_contents($fileFromUpload);
$csv = new CSVImport($csvImportedContent);

if(post_request('headers')){
    $csv->setHeaders(post_request('headers'));
}

$records = $csv->getAll();
$headers = $csv->getHeaders();
$sourceHeaders = $csv->getSourceHeaders();

StoredCSV::set($csv);

?>


<?php html_head(); ?>
 
 <br/><br/>
 
 <h1> CSV Import</h1>
 
 <form method="POST" action="save-map.php">
 
    <?php use_token(); ?>
 
     <table class="table table-striped">
     <thead>
         <tr>
             <th scope="col">Column</th>
             <th scope="col">Imported Column</th>
         </tr>
     </thead>
     <tbody>
         <?php foreach($headers as $colKey=>$column): ?>  
             <tr>
             <th scope="row"><?=$column;?></th>
                 <td>
                     <select class="custom-select" id="inputGroupSelect02" name="order[]">
                        <option></option>
                        <?php foreach($sourceHeaders as $key=>$row): ?>
                            <option value="<?=$row;?>" <?= ($key===$colKey) ? "selected" : ""; ?> ><?=$row;?></option>
                        <?php endforeach; ?>
                     </select>      
                 </td>
             </tr>
         <?php endforeach; ?>    
     </tbody>
     </table>
     <button class="btn btn-primary" type="submit">Download</button>
 </form>
<br/>
 <a href="<?php route('index'); ?>" class="btn btn-secondary">Back</a>
 
 <?php html_footer(); ?>
 