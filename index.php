<?php
require 'vendor/autoload.php';
?>

<?php html_head(); ?>

<h1>Upload</h1>


<?php 
$message = message();
if($message): ?>
<div class="alert alert-primary" role="alert">
    <?=$message;?>
</div>
<?php endif; ?>
<form method="POST" action="map.php" enctype="multipart/form-data">


  <div class="form-group">
    <label for="exampleInputEmail1">Upload CSV</label>
    <input type="file" name="file" class="form-control">
  </div>
  
  <div class="form-group">
    <label for="exampleInputEmail1">Headers</label>
    <textarea name="headers" class="form-control" id=""  rows="5"></textarea>
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
<?php html_footer(); ?>